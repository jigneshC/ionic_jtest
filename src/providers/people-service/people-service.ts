import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
/*
  Generated class for the PeopleServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PeopleServiceProvider {

  apiUrl = 'http://127.0.0.1/be-help/public/api/v1.0/user/register';
  headers: any;
  options: RequestOptions;
  
  constructor(public http: HttpClient,public alertCtrl: AlertController) {
      this.headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });
      this.headers.append('Accept', 'application/json');
   //   this.options = new RequestOptions({ headers: this.headers });
      
  }
  // Search for github users  
  submit(myData: any): Observable<any> {
    return this.http.post(this.apiUrl,myData,{ headers: this.headers }) 
    .map(this.extractData)
    .catch(this.handleError);
  }
    private extractData(res: Response) {
      let result = res;
      
      console.log(result);
      return result || {};
  }

  private handleError(error: any) {
      let errMsg = (error.message) ? error.message :
          error.status ? `${error.status} - ${error.statusText}` : 'Server error';
      console.error(errMsg);
      return Observable.throw(errMsg);
  }
}
