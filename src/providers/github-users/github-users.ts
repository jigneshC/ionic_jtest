import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { User } from '../../models/user';

/*
  Generated class for the GithubUsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GithubUsersProvider {

  githubApiUrl = 'https://api.github.com';
  constructor(private http: HttpClient) {
    console.log('Hello GithubUsersProvider Provider');
  }

  load(): Observable<User[]> {
    return this.http.get(`${this.githubApiUrl}/users`)
      .map(res => <User[]>res);
  }
  loadDetails(login: string): Observable<User> {
    return this.http.get(`${this.githubApiUrl}/users/${login}`)
      .map(res => <User>(res))
  }

   // Search for github users  
  /* searchUsers(searchParam: string): Observable<User[]> {
    return this.http.get(`${this.githubApiUrl}/search/users?q=${searchParam}`) 
    .map((res: Response) => res)
   }*/

  // Search for github users  
  /*searchUsers2(searchParam: string): Observable<User[]> {
    return this.http.get(`${this.githubApiUrl}/search/users?q=${searchParam}`) 
      .map((res: Response) => {
        return res.items;
      }).catch(this.handleError);
  }*/

  getService(): Observable<any> {
    return this.http
        .get(`${this.githubApiUrl}/users`)
        .map(this.extractData)
        .catch(this.handleError);
 }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
}

private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
}

}
