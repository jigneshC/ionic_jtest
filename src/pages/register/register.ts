import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {  PeopleServiceProvider } from '../../providers/people-service/people-service';
import { UsernameValidator } from  '../../validation/username';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  data:any = {};
  files: FileList; 
    filestring: string; 

    slideOneForm: FormGroup;
    slideTwoForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,public peopleServiceProvider: PeopleServiceProvider,public alertCtrl: AlertController,public formBuilder: FormBuilder) {
    this.data.email = "jtest@mail.com";
    this.data.first_name = "jtes";
    this.data.last_name = "com";
    this.data.lang = "fr";
    this.data.password = "123456";

    this.slideOneForm = formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      lang: ['',Validators.required],
      password: ['',Validators.required],
      email: ['',Validators.compose([Validators.required,UsernameValidator.isValid])],
      myfile: ['']
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  getFiles(event) { 
    this.files = event.target.files; 
    var reader = new FileReader(); 
    reader.onload = this._handleReaderLoaded.bind(this); 
    reader.readAsBinaryString(this.files[0]); 
  }
  _handleReaderLoaded(readerEvt) { 
    var binaryString = readerEvt.target.result; 
    this.filestring = btoa(binaryString);  // Converting binary string data. 
  }  

  submit(myData) {

    if(!this.slideOneForm.valid){
        console.log("ss");
        console.log(myData.value);

      //  return false;
    }
      
    

    /*let myData = new FormData();
    myData.append('first_name', this.data.first_name);
    myData.append('last_name', this.data.last_name);
    myData.append('email', this.data.email);
    myData.append('password',this.data.password);
    myData.append('lang', this.data.lang);*/

    // var myData = JSON.stringify({first_name: this.data.first_name,last_name: this.data.last_name,email: this.data.email,password: this.data.password,lang: this.data.lang});
   /* myData.value.base64_image = this.filestring;
    
    this.peopleServiceProvider.submit(myData.value).subscribe(result => {
      if(result.status=="false"){
        let alert = this.alertCtrl.create({
          title: 'Error !',
          subTitle: result.messages,
          buttons: ['OK']
        });
        alert.present();
      }else{
        let alert = this.alertCtrl.create({
          title: 'Success !',
          subTitle: result.messages,
          buttons: ['OK']
        });
        alert.present();
      }
    })*/
  }
}
